---
layout: home
nav_title: Home
---

<strong>Calyx<span>OS</span></strong> is an Android operating system focused on security and privacy. With CalyxOS, your device is preconfigured with the best security tools available, and without all the tracking and surveillance built into a normal Android device.

<strong>Calyx<span>OS</span></strong> is a collaborative open source project sponsored by the <a href="https://www.calyxinstitute.org">Calyx Institute</a>, a 501(c) non-profit dedicated to making security and privacy available for all.
